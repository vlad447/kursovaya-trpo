﻿using System;

namespace LibPollutionformules
{
    // класс входных значений
    public class PollutionInputClass
    {
        public PollutionInputClass(double _H = 0, double _D = 0, int _Tr = 0, int _Ta = 0, double _Zo = 0, double _V = 0, int _A = 0, int _nu = 0, double Velocity = 0)
        {
            H = _H;
            D = _D;
            Tr = _Tr;
            Ta = _Ta;
            Zo = _Zo;
            V = _V;
            A = _A;
            nu = _nu;

        }
        public double H, D, Zo, V, Velocity;
        public int Tr, Ta, A, nu, E;
    }
    // класс выходных 
    public class AirPollutionCountClass
    {


        public PollutionInputClass AirPollutionInput;
        //массив расстояний по Х
        public double[] X = new double[5] { 1000, 3000, 5000, 10000, 15000 };
        //массив скоростей
        public double[] u = new double[4] { 1, 2, 4, 6 };
        //массив расстояний по У
        public double[] Y = new double[5] { 100, 200, 300, 400, 500 };

        private double n, d, Xm, um, f;

        public double Wo_AverageVelocityOutfall (double V, double D)
        {
            return Math.Round(4 * (V / 3600) / (3.14 * Math.Pow(D, 2)), 4);
        }

        public double V1_MixtureVolume (double D)
        {
            return Math.Round(3.14 * Math.Pow(D, 2) * Wo_AverageVelocityOutfall(AirPollutionInput.V, D) / 4, 0);
        }

        public double M_DustAmount (double Zo)
        {
            return Math.Round(Zo * V1_MixtureVolume(AirPollutionInput.D), 1);
        }

        public double f_Coefficient (double D, double H, double V, int Tr, int Ta)
        {
            double avo = Wo_AverageVelocityOutfall(V, D);
            return Math.Round((D * (Math.Pow(avo, 2) * 1000)) / (Math.Pow(H, 2) * (Tr - Ta)), 3);
        }

        public double m_Coefficient (double D, double H, double V, int Tr, int Ta)
        {
            double f_coef = Math.Sqrt(f_Coefficient(D, H, V, Tr, Ta));
            return Math.Round(Math.Pow(0.67 + 0.1 * f_coef + 0.34 * f_coef, -1), 2);
        }

        public double Vm_Coefficient(double D, int Tr, int Ta, double H)
        {
            return Math.Round((0.65 * Math.Pow(V1_MixtureVolume(D) * (Tr - Ta) / H, 1.0 / 3.0)), 2);
        }

        public double n_Coefficient (double D, double H, int Tr, int Ta)
        {
            double vm_coef = Vm_Coefficient(D, Tr, Ta, H);

            if (vm_coef <= 0.3)
            {
                n = 3;
            }
            if (vm_coef > 0.3 & vm_coef <= 2)
            {
                n = (3 - Math.Sqrt((vm_coef - 0.3) * (4.36 - vm_coef)));
            }
            if (vm_coef > 2)
            {
                n = 1;
            }
            return n;
        }

        public double F_Coefficient (int E, int nu)
        {
            if (E == 1)
            {
                f = 1;
            }
            if (E == 0 && nu >= 90)
            {
                f = 2;
            }
            if (E == 0 && nu < 75)
            {
                f = 3;
            }
            if (E == 0 && nu >= 75 && nu < 90)
            {
                f = 2.5;
            }
            return f;
        }

        public double Cm_MaxPollutionConcentration(double Zo, double D, double H, double V, int E, int nu, int Tr, int Ta, int A)
        {
            double coef_multiply = F_Coefficient(E, nu) * m_Coefficient(D, H, V, Tr, Ta) * n_Coefficient(D, H, Tr, Ta);
            return Math.Round((A * M_DustAmount(Zo) * coef_multiply) / (Math.Pow(H, 2) * Math.Pow(V1_MixtureVolume(D) * (Tr - Ta), 1.0 / 3.0)), 2);
        }
        public double Cm_MaxPollutionConcentration(PollutionInputClass arguments) //перегрузка метода
        {
            double coef_multiply = F_Coefficient(arguments.E, arguments.nu) * 
                m_Coefficient(arguments.D, arguments.H, arguments.V, arguments.Tr, arguments.Ta) * 
                n_Coefficient(arguments.D, arguments.H, arguments.Tr, arguments.Ta);

            return Math.Round((arguments.A * M_DustAmount(arguments.Zo) * coef_multiply) / (Math.Pow(arguments.H, 2) * 
                Math.Pow(V1_MixtureVolume(arguments.D) * (arguments.Tr - arguments.Ta), 1.0 / 3.0)), 2);
        }

        public double d_Coefficient(double D, double H, double V, int Tr, int Ta)
        {
            if (Vm_Coefficient(D, Tr, Ta, H) <= 2)
            {
                d = 4.95 * Vm_Coefficient(D, Tr, Ta, H) * (1 + 0.28 * Math.Pow(f_Coefficient(D, H, V, Tr, Ta), 1.0 / 3.0));
            }
            if (Vm_Coefficient(D, Tr, Ta, H) > 2)
            {
                d = 7 * Math.Sqrt(Vm_Coefficient(D, Tr, Ta, H)) * (1 + 0.28 * Math.Pow(f_Coefficient(D, H, V, Tr, Ta), 1.0 / 3.0));
            }
            return Math.Round(d, 2);
        }

        public double Xm_TorchDistance(double D, double H, double V, int E, int nu, int Tr, int Ta)
        {
            if (F_Coefficient(E, nu) >= 2)
            {
                Xm = H * d_Coefficient(D, H, V, Tr, Ta) * ((5 - F_Coefficient(E, nu)) / 4);
            }
            if (F_Coefficient(E, nu) < 2)
            {
                Xm = H * d_Coefficient(D, H, V, Tr, Ta);
            }
            return Xm;
        }
        public double Xm_TorchDistance(PollutionInputClass arg) //еще одна перегрузка для удобства
        {
            if (F_Coefficient(arg.E, arg.nu) >= 2)
            {
                Xm = arg.H * d_Coefficient(arg.D, arg.H, arg.V, arg.Tr, arg.Ta) * ((5 - F_Coefficient(arg.E, arg.nu)) / 4);
            }
            if (F_Coefficient(arg.E, arg.nu) < 2)
            {
                Xm = arg.H * d_Coefficient(arg.D, arg.H, arg.V, arg.Tr, arg.Ta);
            }
            return Xm;
        }



        public double um_DangerousWindVelocity(double D, double H, double V, int Tr, int Ta)
        {
            if (Vm_Coefficient(D, Tr, Ta, H) > 2)
            {
                um = Math.Round(Vm_Coefficient(D, Tr, Ta, H) * (1 + 0.12 * Math.Sqrt(f_Coefficient(D, H, V, Tr, Ta))), 2);
            }
            if (Vm_Coefficient(D, Tr, Ta, H) > 0.5 & Vm_Coefficient(D, Tr, Ta, H) <= 2)
            {
                um = Vm_Coefficient(D, Tr, Ta, H);
            }
            if (Vm_Coefficient(D, Tr, Ta, H) <= 0.5)
            {
                um = Math.Round(0.5 * Vm_Coefficient(D, Tr, Ta, H), 2);
            }
            return um;
        }



        public double[] r_CoefficientArray = new double[4];
        public double[] r_CoefficientCount(double D, double H, double V, int Tr, int Ta)
        {
            double um_dwv = um_DangerousWindVelocity(D, H, V, Tr, Ta);
            for (int i = 0; i < r_CoefficientArray.Length; i++)
            {
                if ((u[i] / um_dwv) < 1)
                {
                    r_CoefficientArray[i] = Math.Round(0.67 * (u[i] / um_dwv) + 1.67 * Math.Pow(u[i] / um_dwv, 2) - 1.34 * Math.Pow(u[i] / um_dwv, 3), 3);
                }
                if ((u[i] / um_dwv) > 1)
                {
                    r_CoefficientArray[i] = Math.Round(3 * (u[i] / um_dwv) / (2 * Math.Pow((u[i] / um_dwv), 2) - (u[i] / um_dwv) + 2), 3);
                }
            }
            return r_CoefficientArray;
        }



        public double[] Cmv_MaxPollutionConcetrationVelocityArray = new double[4];
        public double[] Cmv_MaxPolutionConcentrationVelocityCount(PollutionInputClass arg)
        {
            for (int i = 0; i < Cmv_MaxPollutionConcetrationVelocityArray.Length; i++)
            {
                Cmv_MaxPollutionConcetrationVelocityArray[i] = Math.Round(r_CoefficientCount(arg.D, arg.H, arg.V, arg.Tr, arg.Ta)[i] * Cm_MaxPollutionConcentration(arg), 4);
            }
            return Cmv_MaxPollutionConcetrationVelocityArray;
        }



        public double[] Xmu_MaxConcentrationDistanceArray = new double[4];
        public double[] Xmu_MaxConcentrationDistanceCount(PollutionInputClass arg)
        {
            double xm_td = Xm_TorchDistance(arg);
            double um_dwv = um_DangerousWindVelocity(arg.D, arg.H, arg.V, arg.Tr, arg.Ta);
            for (int i = 0; i < Xmu_MaxConcentrationDistanceArray.Length; i++)
            {
                if ((u[i] / um) < 0.25)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round(3 * xm_td, 0);
                }
                if ((u[i] / um) < 1 && (u[i] / um) >= 0.25)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round((8.43 * Math.Pow(1 - (u[i] / um_dwv), 5) + 1) * xm_td, 0);
                }
                if ((u[i] / um) > 1)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round((0.32 * (u[i] / um_dwv) + 0.68) * xm_td, 0);
                }
            }
            return Xmu_MaxConcentrationDistanceArray;
        }
        public double[] S1_CoefficientArray = new double[5];
        public double[] S1_CoefficientCount(PollutionInputClass arg)
        {
            double xm_td = Xm_TorchDistance(arg);
            for (int i = 0; i < S1_CoefficientArray.Length; i++)
            {
                if ((X[i] / xm_td) <= 1)
                {
                    S1_CoefficientArray[i] = (3 * Math.Pow((X[i] / xm_td), 4) - 8 * Math.Pow((X[i] / xm_td), 3) + 6 * Math.Pow((X[i] / xm_td), 2));
                }
                if ((X[i] / xm_td) <= 8 && (X[i] / xm_td) > 1)
                {
                    S1_CoefficientArray[i] = (1.13 / (0.13 * Math.Pow((X[i] / xm_td), 2) + 1));
                }
                if ((X[i] / xm_td) > 8)
                {
                    S1_CoefficientArray[i] = (Math.Pow(0.1 * Math.Pow((X[i] / xm_td), 2) + 2.47 * (X[i] / xm_td) - 17.8, -1));
                }

            }
            return S1_CoefficientArray;
        }
        public double[] Cx_ConcentrationCountOfDistanceArray = new double[5];
        public double[] Cx_ConcentrationCountOfDistanceCount(PollutionInputClass arg)
        {
            for (int i = 0; i < Cx_ConcentrationCountOfDistanceArray.Length; i++)
            {
                Cx_ConcentrationCountOfDistanceArray[i] = (S1_CoefficientCount(arg)[i] * Cm_MaxPollutionConcentration(arg));
            }
            return Cx_ConcentrationCountOfDistanceArray;
        }
        public double[,] S2_CoefficientArray = new double[5, 5];

        public double[,] S2_CoefficientCount()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    S2_CoefficientArray[i, j] = Math.Pow((1d + 8.4d * AirPollutionInput.Velocity * Math.Pow((Y[i] / X[j]), 2)) * (1d + 28.2d * Math.Pow(u[0], 2) * Math.Pow((Y[i] / X[j]), 4)), -1);
                }
            }
            return S2_CoefficientArray;
        }

        public double[,] Cy_ConcentrationCountOfDistanceArray = new double[5, 5];
        public double[,] Cy_ConcentrationCountOfDistanceCount(PollutionInputClass arg)
        {

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Cy_ConcentrationCountOfDistanceArray[j, i] = (S2_CoefficientCount()[i, j] * Cx_ConcentrationCountOfDistanceCount(arg)[j]);
                }
            }
            return Cy_ConcentrationCountOfDistanceArray;
        }

    }
}